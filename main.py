import json, requests, pprint

username = "demo"
password = "demo"
api_host = "https://apis.insylo.io"

def get_new_api_token():
    url = api_host + '/v2/login/'
    data = json.dumps({
            'username': username,
            "password": password
        }
    )
    response = requests.post(
        url, data=data,
        headers={"Content-Type": "application/json",
        }
    )
    if response.status_code == 200:
        return response.json().get("token")


class Insylo(object):

    def __init__(self, token):
        self.token = token


    def get_areas(self):
        """ Gives the Areas assigned to the user
        {
          "data": [
            {
              "contactName": "string",
              "contactNumber": "string",
              "createdBy": "string",
              "description": "string",
              "id": "string",
              "label": "string",
              "location": {
                "city": "string",
                "coordinates": {
                  "latitude": 0,
                  "longitude": 0
                },
                "country": "string",
                "state": "string",
                "street": "string",
                "zipcode": "string"
              },
              "ownerId": "string",
              "parentId": "string",
              "status": 0,
              "timestamp": {
                "createdAt": "string",
                "updatedAt": "string"
              }
            }
          ],
          "limit": 0,
          "offset": 0,
          "total": 0
        }
        """
        url = api_host + "/v2/areas/"
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()


    def get_bins(self, area_id):
        """Gives the list of Bins assigned to the user
        [
          {
            "data": [
              {
                "areaId": "string",
                "comment": "string",
                "coordinates": {
                  "latitude": 0,
                  "longitude": 0
                },
                "createdBy": "string",
                "daysRemains": 0,
                "deviceUUID": "string",
                "id": "string",
                "images": [
                  "string"
                ],
                "label": "string",
                "meta": {},
                "ownerId": "string",
                "specs": {
                  "coneHeight": 0,
                  "coneTopAngle": 0,
                  "coneTruncated": 0,
                  "cylinderDiameter": 0,
                  "cylinderHeight": 0,
                  "distanceToHole": 0,
                  "informedVolume": 0
                },
                "status": 0,
                "timestamp": {
                  "createdAt": "string",
                  "updatedAt": "string"
                },
                "uuid": "string"
              }
            ],
            "limit": 0,
            "offset": 0,
            "total": 0
          }
        ]
        """
        url = api_host + "/v2/bins/?areaId=" + area_id
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def get_metrics(self, binuuid):
        """Gives the timeseries of the Bin
        [
          {
            "data": [
              {
                "a": "string",
                "accelerometerCamera": "string",
                "accelerometerGround": "string",
                "avgDailyConsumption": 0,
                "battery": "string",
                "ber": "string",
                "binUUID": "string",
                "coverageArea": 0,
                "density": 0,
                "depthMap": "string",
                "depthMapHeight": 0,
                "depthMapImage": "string",
                "depthMapWidth": 0,
                "deviceUUID": "string",
                "filteredVolume": 0,
                "historicalDepthMap": "string",
                "humidity": "string",
                "iccid": "string",
                "id": "string",
                "meta": {},
                "mode": "string",
                "packingFactor": 0,
                "qualityIndex": 0,
                "reg": 0,
                "rgb": "string",
                "rssi": "string",
                "simpleDepthMap": "string",
                "slStatus": 0,
                "temp": "string",
                "timestamp": {
                  "createdAt": "string",
                  "updatedAt": "string"
                },
                "version": "string",
                "volume": 0,
                "weight": 0
              }
            ],
            "limit": 0,
            "offset": 0,
            "total": 0
          }
        ]
        """
        url = api_host + "/v2/bins/{}".format(binuuid)
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

if __name__ == "__main__":
    token = get_new_api_token()
    insylo = Insylo(token)
    # List all the areas.
    areas = insylo.get_areas()
    # Get the Bin
    for area in areas["data"]:
        bins = insylo.get_bins(area["id"])
        # Get all metrics of the bins
        for bi in bins["data"]:
            metrics = insylo.get_metrics(bi["uuid"])
            pprint.pprint(metrics)
